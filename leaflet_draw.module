<?php
/**
 * @file
 * Provides library functions for the Leaflet Draw module.
 */

// A seperate file for the widet hooks
require_once('leaflet_draw.widgets.inc');

/**
 * Implements hook_library().
 */
function leaflet_draw_library() {
  $libraries['leaflet.draw'] = array(
    'title' => 'Leaflet',
    'website' => 'https://github.com/jacobtoye/Leaflet.draw',
    'version' => '0.1',
    'js' => array(
      array(
        'type' => 'file',
        'data' => libraries_get_path('leaflet.draw') . '/dist/leaflet.draw-src.js',
        'group' => JS_LIBRARY,
        'preprocess' => FALSE,
      ),
    ),
    'css' => array(
      libraries_get_path('leaflet.draw') . '/dist/leaflet.draw.css' => array(
        'type' => 'file',
        'media' => 'screen',
      ),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_requirements().
 */
function leaflet_draw_requirements($phase) {
  $requirements = array();

  // ensure js library is available
  if ($phase == 'runtime' && !file_exists(libraries_get_path('leaflet.draw') . '/dist/leaflet.draw.js')) {
    $requirements['leaflet.draw'] = array(
      'title' => t('Leaflet.draw library not found'),
      'value' => t('The !leafletdraw javascript library was not found. Please !download it into the libraries folder.',
        array(
          '!leafletdraw' => l('Leaflet.draw', 'https://github.com/jacobtoye/Leaflet.draw'),
          '!download' => l('download', 'https://github.com/jacobtoye/Leaflet.draw'),
        )
      ),
      'severity' => REQUIREMENT_ERROR,
    );
  }

  return $requirements;
}