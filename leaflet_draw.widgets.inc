<?php
/**
 * @file
 * Provides field widget hooks for Leaflet Draw module.
 */


/**
 * Implements hook_field_widget_info().
 */
function leaflet_draw_field_widget_info() {
  $widgets = array();

  $widgets['leaflet_draw'] = array(
    'label' => t('Leaflet Draw'),
    'field types' => array('geofield'),
  );

  return $widgets;
}


/**
 * Implements hook_field_widget_settings_form().
 */
function leaflet_draw_field_widget_settings_form($field, $instance) {
  // Get the settings.
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  if ($widget['type'] == 'leaflet_draw') {
    $form = array();

    // Default JSON settings.
    $form['leaflet_draw_map_alter_json'] = array(
      '#title' => t('Alter the map with json'),
      '#description' => t('Something like: <strong>
        { "zoom": 18, "minZoom": 0, "maxZoom": 18, "dragging": true }</strong><br />  <em>See the <a href="http://leaflet.cloudmade.com/reference.html#map-options">Leaflet API</a> for more information.</em>'),
      '#type' => 'textarea',
      '#default_value' => isset($settings['leaflet_draw_map_alter_json']) ? $settings['leaflet_draw_map_alter_json'] : '',
    );

    // Center latitude.
    $form['leaflet_draw_center_lat'] = array(
      '#type' => 'textfield',
      '#title' => t('Default center latitude'),
      '#default_value' => isset($settings['leaflet_draw_center_lat']) ? $settings['leaflet_draw_center_lat'] : 0,
    );

    // Center longitude.
    $form['leaflet_draw_center_lon'] = array(
      '#type' => 'textfield',
      '#title' => t('Default center longitude'),
      '#default_value' => isset($settings['leaflet_draw_center_lon']) ? $settings['leaflet_draw_center_lon'] : 0,
    );

    // Zoom level.
    $form['leaflet_draw_zoom'] = array(
      '#type' => 'textfield',
      '#title' => t('Default zoom level'),
      '#default_value' => isset($settings['leaflet_draw_zoom']) ? $settings['leaflet_draw_zoom'] : 0,
    );

    // The Leaflet.draw tools.
    $tool_options = array(
      'polyline' => t('Polyline'),
      'marker' => t('Marker'),
      'rectangle' => t('Rectangle'),
      'circle' => t('Circle'),
      'polygon' => t('Polygon'),
    );

    // Leaflet tool selector.
    $form['leaflet_draw_tools'] = array(
      '#type' => 'checkboxes',
      '#required' => TRUE,
      '#options' => $tool_options,
      '#default_value' => isset($settings['leaflet_draw_tools']) ? $settings['leaflet_draw_tools'] : array('marker'),
    );

    // Get all maps of the site, prepare them for a select.
    foreach (leaflet_map_get_info() as $key => $map) {
      $map_options[$key] = $map['label'];
    }

    // Leaflet map selector.
    $form['leaflet_draw_map'] = array(
      '#type' => 'select',
      '#options' => $map_options,
      '#default_value' => isset($settings['leaflet_draw_map']) ? $settings['leaflet_draw_map'] : leaflet_map_get_info('OSM Mapnik'),
    );

    return $form;
  }
}


/**
 * Implements hook_field_widget_form().
 */
function leaflet_draw_field_widget_form(&$form, &$form_state, $field, $instance,
  $langcode, $items, $delta, $base) {

  $widget = $instance['widget'];
  $settings = $instance['widget']['settings'];
  $element = geofield_get_base_element($base, $items, $delta);

  // Include the wkt field, here we will put the values in.
  $element['wkt']['#title'] = 'Well Known Text';
  $element['wkt']['#type'] = 'textarea';
  $element['wkt']['#attributes']['class'][] = 'hidden-wkt-leaflet-draw';

  // Get the settings for the leaflet map.
  $map = leaflet_map_get_info($settings['leaflet_draw_map']);

  $map_settings_decoded = (array) json_decode($settings['leaflet_draw_map_alter_json']);

  // Set the default zoom.
  if (isset($settings['leaflet_draw_zoom'])) {
    $map['settings']['zoom'] = $settings['leaflet_draw_zoom'];
  }

  // Merge json settings with the map settings.
  if ($map_settings_decoded) {
    $map['settings'] = array_merge($map['settings'], $map_settings_decoded);
  }

  // Add the center to the map
  if (isset($items[$delta]['lat']) && isset($items[$delta]['lon'])) {
    $map['settings']['center'] = array(
      'lat' => $items[$delta]['lat'],
      'lng' => $items[$delta]['lon']
    );
  // Fallback to the widget default.
  } else if (isset($settings['leaflet_draw_center_lat']) && isset($settings['leaflet_draw_center_lon'])) {
    $map['settings']['center'] = array(
      'lat' => $settings['leaflet_draw_center_lat'],
      'lng' => $settings['leaflet_draw_center_lon'],
    );
  }
  // Fallback to a default, else leaflet won't render see http://drupal.org/node/1799154.
  else {
    $map['settings']['center'] = array(
      'lat' => 39.50,
      'lng' => -98.35,
    );
  }

  // Render the map.
  $element['map'] = array(
    '#markup' => '<label>' . $instance['label'] . '</label>' . leaflet_render_map($map),
  );

  drupal_add_library('leaflet_draw', 'leaflet.draw');

  // Without the weight Leaflet crashes.
  drupal_add_js(drupal_get_path('module', 'leaflet_draw') . '/js/leaflet_draw_input.js', array(
    'weight' => 1600,
  ));

  // In the after build we can check what html id has been assigned to the textfield.
  $element['#after_build'][] = 'leaflet_draw_field_widget_after_build';

  return $element;
}


/**
 * Implements a FAPI #after_build.
 */
function leaflet_draw_field_widget_after_build($element, &$form_state) {
  $settings = $form_state['field'][$element['#field_name']][$element['#language']]['instance']['widget']['settings'];

  // Parse the rendered map variable so we can get the leaflet map id.
  // This shouldn't be done this way. TODO make this better in the leaflet module.
  $map_id_exploded = explode('<div id="', $element['map']['#markup']);
  $map_id_exploded_part_two = explode('"', $map_id_exploded[1]);
  $map_id = $map_id_exploded_part_two[0];

  // Make FAPI checkboxes values usable for javascript.
  foreach($settings['leaflet_draw_tools'] as $key => $tool) {
    if ($key === $tool) {
      $leaflet_draw_tools[$key] = TRUE;
    }
    else {
      $leaflet_draw_tools[$key] = FALSE;
    }
  }

  // Add the javascript settings to Drupal.settings.
  drupal_add_js(array('leaflet_draw' => array(
    $map_id => array(
      'map' => $map_id,
      'tools' => $leaflet_draw_tools,
      'textfield' => $element['wkt']['#id'],
    ))), 'setting');

  return $element;
}
