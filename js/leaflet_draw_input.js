(function ($) {

  Drupal.behaviors.leafletDraw = {
    attach: function (context, settings) {
      if (Drupal.settings.leaflet_draw) {

        var map = null;
        var settings = null;

        // Hides the wkt form element
        //$('.hidden-wkt-leaflet-draw').parents('.form-item').hide();

        $.each(Drupal.settings.leaflet_draw, function(index, valueDraw) {

          $.each(Drupal.settings.leaflet, function(key, value) {
            if (value.mapId == valueDraw.map) {
              map = Drupal.settings.leaflet[key].lMap;
              settings = valueDraw;
            }
          });

          var drawControl = new L.Control.Draw({
              polyline: settings.tools.polyline,
              rectangle: settings.tools.rectangle,
              circle: settings.tools.circle,
              polygon: settings.tools.polygon,
              marker: settings.tools.marker
          });

          map.addControl(drawControl);

          Drupal.behaviors.leafletDraw.drawnItems = new L.LayerGroup();

          map.on('draw:marker-created', function (e) {
            Drupal.behaviors.leafletDraw.drawnItems.clearLayers();
            Drupal.behaviors.leafletDraw.drawnItems.addLayer(e.marker);

            var latLng = e.marker.getLatLng();
            $('#' + settings.textfield).val('POINT (' + latLng.lng + ' ' + latLng.lat + ')');
          });

          // Set the current features on the map
          if (Drupal.settings.leaflet_draw !== undefined && Drupal.settings.leaflet_draw.features !== undefined) {
            L.geoJson(Drupal.settings.leaflet_draw.features).addTo(Drupal.behaviors.leafletDraw.drawnItems);
          }

          map.addLayer(Drupal.behaviors.leafletDraw.drawnItems);
        });
      }
    }
  };

})(jQuery);
